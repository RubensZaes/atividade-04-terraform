// Configura a versão do Terraform e a versão do provedor Google Cloud
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.73.0"
    }
  }
  required_version = ">= 0.15.0"
}

// Configura o provedor Google Cloud com as definições de projeto, região e zona
provider "google" {
  credentials = "${file("curso-devops-398119-38fb82838c1a.json")}"
  project = "curso-devops-398119" // Substitua pelo seu ID de projeto real - https://console.cloud.google.com/
  region  = "us-central1"
  zone    = "us-central1-c"
}

// Define um recurso de instância do Google Compute Engine
resource "google_compute_instance" "vm_instance" {
  name         = "instancia-terraform"
  machine_type = "e2-micro"

  // Configura o disco de inicialização com a imagem especificada
  boot_disk {
    initialize_params {
      image = "debian-10"
    }
  }

  // Adiciona chave publica na VM para conexao SSH
  #metadata = {
  #  ssh-keys = "NOMEUSUARIOCONTA:${file("./id_rsa.pub")}" //substituir pelo nome do usuario da conta (usuário do e-mail sem o @gmail.com) e o nome da chave publica correspondente
  #}

  // Conecta a instância à rede padrão
  network_interface {
    network = "default"
    access_config {
      // Deixa access_config vazio para usar um IP efêmero
    }
  }

  // Define um script de inicialização para a instância instalar o Apache e exibir uma mensagem
  metadata_startup_script = "sudo apt-get update && sudo apt-get install apache2 -y && echo '<!doctype html><html><body><h1>Este texto prova que o Terraform funciona!!</h1></body></html>' | sudo tee /var/www/html/index.html"

  // Aplica a tag "http-server" à instância para associação com a regra de firewall
  tags = ["http-server"]
}

// Define um recurso de regra de firewall no Google Compute Engine para permitir tráfego HTTP
resource "google_compute_firewall" "http-server" {
  name    = "permitir-http-padrao-terraform"
  network = "default"

  // Permite tráfego TCP de entrada na porta 80
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  // Permite tráfego de todas as origens para instâncias com a tag "http-server"
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}

// Define uma variável de saída para exibir o endereço IP externo da instância
output "ip" {
  value = google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip
}
